package routes

import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.http.Context

data class Move (val description: String ="", val type: String = "")

fun createRoutes(){

    val app = Javalin.create { config ->
        config.defaultContentType = "application/json"
        config.dynamicGzip = true
//        config.enforceSsl = true
        config.contextPath = "/api/v1"
    }.routes{

//       get("/move/:move"){ctx: Context -> ctx.json(MoveRequestHandler::getMoveByName)}
        get("/move/:move"){ctx: Context -> ctx.json("BLAH")}
    }

    app.error(404) { ctx->
        ctx.result("YOU DONE GOOFED, 404 MAN!!!")
    }.start(1991)
}

class MoveRequestHandler {
    private val moveDAO = MoveDAO()

    fun getMoveByName(ctx: Context):Move {
        val moveName = ctx.pathParam("move")
        println(moveName)
        return moveDAO.getMoveByName(moveName)
    }
}


class MoveDAO {

    private val moves = hashMapOf(
        "windmill" to Move(
            "A classic bboy move where the dancer spins around the crown of their head with their legs out",
            "Power Move"
        ),
        "flare" to Move("A classic power move where the dancer throws both legs out in a circle similar to gymnast circles, but with the legs open", "Air Power"),
        "toprock" to Move(),
        "" to Move()
    )

    fun getMoveByName(moveName: String): Move {
        return moves.getValue(moveName)
    }
}
